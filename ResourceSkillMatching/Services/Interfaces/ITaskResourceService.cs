﻿using System.Collections.Generic;
using ResourceSkillMatching.ViewModels;

namespace ResourceSkillMatching.Services.Interfaces
{
    /// <summary>
    /// Contains methods for task resource
    /// </summary>
    public interface ITaskResourceService
    {
        IEnumerable<TaskResourceVM> GetAvailableResourcesForTask(int projectId);
    }
}