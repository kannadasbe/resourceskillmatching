﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResourceSkillMatching.Models;
using ResourceSkillMatching.Constants;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity.Infrastructure;
using ResourceSkillMatching.ViewModels;
using System.Data.Entity.Core.Objects;
using ResourceSkillMatching.Repositories.Interface;
using ResourceSkillMatching.Repositories;
using ResourceSkillMatching.Data.Interfaces;
using ResourceSkillMatching.Data;
using ResourceSkillMatching.Services.Interfaces;

namespace ResourceSkillMatching.Services
{
    public class TaskResourceService : ITaskResourceService
    {
        private readonly IUnitOfWork _unitOfWork;
        public TaskResourceService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Returns Available Resources For Project Tasks
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<TaskResourceVM> GetAvailableResourcesForTask(int projectId)
        {
            var taskAndResourceList = _unitOfWork.TaskResourceRepository.GetAvailableResourcesForTask(projectId);
            var taskResources = new List<TaskResourceVM>();

            //Loop through Tasks & Resources and construct the target object List<TaskResourceVM>
            TaskVM taskVM; List<ResourceVM> resourceList;
            foreach (var task in taskAndResourceList.Tasks)
            {
                taskVM = new TaskVM() { TaskId = task.TaskId, TaskName = task.TaskName, StartDate = task.StartDate, EndDate = task.EndDate };
                resourceList = new List<ResourceVM>();

                //Find matching resources for the task
                if (!string.IsNullOrWhiteSpace(task.ResourceIds))
                {
                    var resourceArray = task.ResourceIds.Split(',').Select(Int32.Parse).ToList();
                    resourceList = (from a in taskAndResourceList.Resources
                                    join b in resourceArray on a.ResourceId equals b
                                    select new ResourceVM() { ResourceId = a.ResourceId, ResourceName = a.ResourceName })?.ToList();
                }
                taskResources.Add(new TaskResourceVM() { Task = taskVM, MatchingResources = resourceList });
            }
            return taskResources;
        }
    }
}