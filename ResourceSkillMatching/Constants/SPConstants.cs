﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceSkillMatching.Constants
{
    /// <summary>
    /// Constants file to store stored procedure names
    /// </summary>
    public static class SPConstants
    {
        public const string AvailableResourcesForTaskSP= "dbo.GetProjectTasksWithResources";
    }
}