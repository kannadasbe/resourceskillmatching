namespace ResourceSkillMatching.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SkillMaster")]
    public partial class SkillMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SkillMaster()
        {
            ResourceSkills = new HashSet<ResourceSkill>();
            TaskSkills = new HashSet<TaskSkill>();
        }

        [Key]
        public int SkillId { get; set; }

        [StringLength(500)]
        public string SkillName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ResourceSkill> ResourceSkills { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskSkill> TaskSkills { get; set; }
    }
}
