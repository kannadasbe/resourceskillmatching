namespace ResourceSkillMatching.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProjectTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProjectTask()
        {
            TaskSkills = new HashSet<TaskSkill>();
        }

        [Key]
        public int TaskId { get; set; }

        [StringLength(500)]
        public string TaskName { get; set; }

        public int? ProjectId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }

        public int? ResourceId { get; set; }

        public virtual ProjectMaster ProjectMaster { get; set; }

        public virtual ResourceMaster ResourceMaster { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskSkill> TaskSkills { get; set; }
    }
}
