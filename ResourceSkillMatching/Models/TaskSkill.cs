namespace ResourceSkillMatching.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskSkill
    {
        public int TaskSkillId { get; set; }

        public int? TaskId { get; set; }

        public int? SkillId { get; set; }

        public virtual ProjectTask ProjectTask { get; set; }

        public virtual SkillMaster SkillMaster { get; set; }
    }
}
