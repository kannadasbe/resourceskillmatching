namespace ResourceSkillMatching.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ResourceSkill
    {
        public int ResourceSkillId { get; set; }

        public int? ResourceId { get; set; }

        public int? SkillId { get; set; }

        public virtual ResourceMaster ResourceMaster { get; set; }

        public virtual SkillMaster SkillMaster { get; set; }
    }
}
