﻿using NLog;
using ResourceSkillMatching.Services;
using ResourceSkillMatching.Services.Interfaces;
using ResourceSkillMatching.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ResourceSkillMatching.Controllers
{
    public class TaskResourcesController : ApiController
    {
        private readonly ITaskResourceService _projectTasksService;
        private ILogger _logger = LogManager.GetCurrentClassLogger();

        public TaskResourcesController(ITaskResourceService projectTasksService)
        {
            this._projectTasksService = projectTasksService;
        }

        /// <summary>
        /// Get Task Resources by projectid
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        // GET: api/TaskResources?projectId=1
        public IHttpActionResult Get(int projectId)
        {
            var responseWrapper = new ResponseWrapper<IEnumerable<TaskResourceVM>>();
            _logger.Info("Entering TaskResourceController - Get action. ProjectId - " + projectId.ToString());
            try
            {
                var response = _projectTasksService.GetAvailableResourcesForTask(projectId).ToList();
                responseWrapper.Status = "Success";
                responseWrapper.Response = response;
                //If no tasks found for project return proper message
                if (response.Count() == 0)
                {
                    responseWrapper.Message = string.Format($"No tasks found for ProjectId {projectId}");
                }
            }
            catch (Exception ex)
            {
                //Log error and through relevant exception. 
                _logger.Error(ex, "Error occured in TaskResources controller Get Action");
                responseWrapper.Status = "Error";
                //Return relevant error message
                responseWrapper.Message = "Something went wrong..";
            }
            return Ok(responseWrapper);
        }
    }
}
