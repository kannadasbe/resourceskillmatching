using ResourceSkillMatching.Data;
using ResourceSkillMatching.Data.Interfaces;
using ResourceSkillMatching.Services;
using ResourceSkillMatching.Services.Interfaces;
using System.Web.Http;
using Unity;
using Unity.Injection;
using Unity.WebApi;

namespace ResourceSkillMatching
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            container.RegisterType<RSMDBContext>();

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<ITaskResourceService, TaskResourceService>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}