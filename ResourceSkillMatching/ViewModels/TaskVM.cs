﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceSkillMatching.ViewModels
{
    public class TaskVM
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public int? ProjectId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ResourceIds { get; set; }
    }
}