﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResourceSkillMatching.Models;

namespace ResourceSkillMatching.ViewModels
{
    public class TaskResourceVM
    {
        public TaskVM Task { get; set; }
        public IEnumerable<ResourceVM> MatchingResources { get; set; }
    }
    public class TaskAndResourceListVM
    {
        public IEnumerable<TaskVM> Tasks { get; set; }
        public IEnumerable<ResourceVM> Resources { get; set; }
    }
}