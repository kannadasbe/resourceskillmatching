﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceSkillMatching.ViewModels
{
    public class ResourceVM
    {
        public int ResourceId { get; set; }
        public string ResourceName { get; set; }
    }
}