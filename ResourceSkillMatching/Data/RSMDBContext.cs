namespace ResourceSkillMatching.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using ResourceSkillMatching.Models;

    public partial class RSMDBContext : DbContext
    {
        public RSMDBContext()
            : base("name=DBConnString")
        {
        }

        public virtual DbSet<ProjectMaster> ProjectMasters { get; set; }
        public virtual DbSet<ProjectTask> ProjectTasks { get; set; }
        public virtual DbSet<ResourceMaster> ResourceMasters { get; set; }
        public virtual DbSet<ResourceSkill> ResourceSkills { get; set; }
        public virtual DbSet<SkillMaster> SkillMasters { get; set; }
        public virtual DbSet<TaskSkill> TaskSkills { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectMaster>()
                .Property(e => e.ProjectName)
                .IsUnicode(false);

            modelBuilder.Entity<ProjectTask>()
                .Property(e => e.TaskName)
                .IsUnicode(false);

            modelBuilder.Entity<ResourceMaster>()
                .Property(e => e.ResourceName)
                .IsUnicode(false);

            modelBuilder.Entity<SkillMaster>()
                .Property(e => e.SkillName)
                .IsUnicode(false);
        }
    }
}
