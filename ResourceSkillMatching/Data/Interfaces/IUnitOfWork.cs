﻿using ResourceSkillMatching.Repositories.Interface;

namespace ResourceSkillMatching.Data.Interfaces
{
    public interface IUnitOfWork
    {
        ITaskResourceRepository TaskResourceRepository { get; }

        void Dispose();
        void Save();
    }
}