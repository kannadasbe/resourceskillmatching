﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using ResourceSkillMatching.Data.Interfaces;
using ResourceSkillMatching.Models;
using ResourceSkillMatching.Repositories;
using ResourceSkillMatching.Repositories.Interface;

namespace ResourceSkillMatching.Data
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly RSMDBContext _context;

        public UnitOfWork(RSMDBContext context)
        {
            this._context = context;
        }
        public UnitOfWork()
        {
            this._context = new RSMDBContext();
        }
        private TaskResourceRepository _taskResourceRepository;

        public ITaskResourceRepository TaskResourceRepository => _taskResourceRepository = _taskResourceRepository ?? new TaskResourceRepository(_context);

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}