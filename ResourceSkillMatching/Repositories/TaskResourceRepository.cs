﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResourceSkillMatching.ViewModels;
using ResourceSkillMatching.Models;
using ResourceSkillMatching.Constants;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.SqlClient;
using ResourceSkillMatching.Data;
using ResourceSkillMatching.Repositories.Interface;

namespace ResourceSkillMatching.Repositories
{
    public class TaskResourceRepository : GenericRepository<TaskResourceVM>, ITaskResourceRepository
    {
        public TaskResourceRepository(RSMDBContext context)
            : base(context)
        { }

        /// <summary>
        /// GetAvailableResourcesForTask by projectId
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public TaskAndResourceListVM GetAvailableResourcesForTask(int projectId)
        {
            var taskAndResourceListVM = new TaskAndResourceListVM();
            context.Database.Initialize(force: false);

            // Create a SQL command to execute the sproc
            var cmd = context.Database.Connection.CreateCommand();
            cmd.CommandText = SPConstants.AvailableResourcesForTaskSP;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("ProjectId", SqlDbType.Int) { Value = Convert.ToInt32(projectId) });

            try
            {
                context.Database.Connection.Open();
                // Run the procedure
                var reader = cmd.ExecuteReader();

                // Read tasks from the first result set
                taskAndResourceListVM.Tasks = ((IObjectContextAdapter)context)
                    .ObjectContext
                    .Translate<TaskVM>(reader)?.ToList();

                // Move to second result set and read resources
                reader.NextResult();
                taskAndResourceListVM.Resources = ((IObjectContextAdapter)context)
                    .ObjectContext
                    .Translate<ResourceVM>(reader)?.ToList();
            }
            finally
            {
                //close db connection
                context.Database.Connection.Close();
            }

            //return result
            return taskAndResourceListVM;
        }
    }
}