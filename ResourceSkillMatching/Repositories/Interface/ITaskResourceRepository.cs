﻿using ResourceSkillMatching.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResourceSkillMatching.ViewModels;

namespace ResourceSkillMatching.Repositories.Interface
{
    /// <summary>
    /// Task resource repository
    /// </summary>
    public interface ITaskResourceRepository : IGenericRepository<TaskResourceVM>
    {
        TaskAndResourceListVM GetAvailableResourcesForTask(int projectId);
    }
}